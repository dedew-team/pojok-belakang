from django.test import TestCase, Client
from django.contrib.auth.models import User
from social_django.models import UserSocialAuth
from django.urls import resolve
from profile_page.views import *

class web_Unit_Test(TestCase):
	def test_url_is_exist(self):
		user = User.objects.create(username='testuser', email='lulu.ilmaknun.q@gmail.com')
		user.set_password('12345')
		user.save()
		UserSocialAuth.objects.create(user=user, provider='google-oauth2')
		client = Client()
		client.login(username='testuser', password='12345')

		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		new_object = Donatur.objects.create(program=new_program,
										nama='Lulu', email='lulu.ilmaknun.q@gmail.com', jumlah=1000000, anon=True)

		response = client.get('/profile/')
		found = resolve('/profile/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'profile_page/profile.html')
		self.assertEqual(found.func, profile_page)
