from django.shortcuts import render
from program_detail.models import *

def profile_page(request):
    donation = Donatur.objects.all()
    donation_list = []
    profile = {}

    profile['email'] = request.user.email
    profile['name'] = request.user.get_full_name()

    for donas in donation:
        if donas.email == request.user.email:
            donation_list.append({'title' : donas.program.title, 'jumlah' : donas.jumlah})

    content = {'title' : 'Profile',
                'donasi' : donation_list,
                'profile' : profile}

    return render(request, 'profile_page/profile.html', content)
