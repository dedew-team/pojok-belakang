from django.urls import path
from profile_page import views

app_name = 'profile_page'
urlpatterns = [
    path('', views.profile_page, name='profile_page'),
]
