from django.shortcuts import render
from django.shortcuts import redirect
from django.http import JsonResponse
from program_detail.models import *
from program_detail.forms import *

def prog_detail(request, pk):
	prog = Program.objects.get(pk=pk)
	form = DonateForm()
	donatur = Donatur.objects.all()

	content = {'title' : prog.title,
				'form' : form,
				'prog' : prog,
				'donatur' : donatur}

	return render(request, 'program_detail/prog_detail.html', content)

def donate(request, pk):
	auth_status = request.user.is_authenticated
	if auth_status:
		if request.method == "POST":
			jumlah = request.POST.get('jumlah', None)
			anon = request.POST.get('anon', None)
			anonim = False
			if anon == "true":
				anonim = True
			program = Program.objects.get(pk=pk)
			nama = request.user.first_name
			email =request.user.email
			Donatur.objects.create(nama=nama, program=program, email=email, jumlah=jumlah, anon=anonim)

	context = {
		'authenticated': auth_status
	}
	return JsonResponse(context)
