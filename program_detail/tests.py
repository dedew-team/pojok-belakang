from django.test import TestCase, Client
from django.urls import resolve
from program_detail.views import *
from program_detail.models import *
from program_detail.forms import *
from django.contrib.auth.models import User
from social_django.models import UserSocialAuth

class web_Unit_Test(TestCase):
	def test_url_is_exist(self):
		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		response = Client().get('/prog_detail/1/')
		found = resolve('/prog_detail/1/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'program_detail/prog_detail.html')
		self.assertEqual(found.func, prog_detail)

	def test_donate(self):
		user = User.objects.create(username='testuser', email='lulu.ilmaknun.q@gmail.com')
		user.set_password('12345')
		user.save()
		UserSocialAuth.objects.create(user=user, provider='google-oauth2')
		client = Client()
		client.login(username='testuser', password='12345')

		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')

		response = client.post('/prog_detail/1/donate/', data={'program': new_program, 'jumlah' : 1000000})
		self.assertEqual(response.status_code, 200)

class model_program_Unit_Test(TestCase):
	def test_can_create_object(self):
		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		count_available_object = Program.objects.all().count()
		self.assertEqual(count_available_object, 1)

class model_donatur_Unit_Test(TestCase):
	def test_can_create_object(self):
		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		new_object = Donatur.objects.create(program=new_program,
										nama='Lulu', email='lulu.ilmaknun.q@gmail.com', jumlah=1000000, anon=True)
		count_available_object = Donatur.objects.all().count()
		self.assertEqual(count_available_object, 1)

class donate_Form_Test(TestCase):
	def test_forms_valid(self):
		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		form_data = {'program': new_program.pk, 'nama':'Lulu Ilmaknun Q', 'email': 'lulu.ilmaknun.q@gmail.com', 'jumlah' : 1000000}
		form = DonateForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_forms_not_valid_email(self):
		new_program = Program.objects.create(title='Bantu Saya Melupakan Dia', image_url='http://s5.favim.com/610/51/Favim.com-amazon-box-broken-heart-466275.jpg', desc='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		form_data = {'program': new_program.pk, 'nama':'Lulu Ilmaknun Q', 'email': 'lulu.ilmaknun.q', 'jumlah' : 'x1000000'}
		form = DonateForm(data=form_data)
		self.assertFalse(form.is_valid())
