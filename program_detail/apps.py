from django.apps import AppConfig


class ProgramDetailConfig(AppConfig):
    name = 'program_detail'
