from django import forms
from .models import Donatur
from django.forms import ModelForm

class DonateForm(ModelForm):
	class Meta:
		model= Donatur
		fields = ['jumlah', 'anon']

		widgets = {
			'jumlah' : forms.NumberInput(attrs={'class' : 'form-control',
                                        'type' : 'number',
                                        'placeholder' : 'how much u wan to donate',
										'id': 'form_jumlah'}),
			'anon' : forms.CheckboxInput(attrs={'class': 'checkbox',
										'type' : 'checkbox',
										'id': 'form_anon'})
        }
