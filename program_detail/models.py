from django.db import models
from django.utils.timezone import now

class Program(models.Model):
	title = models.CharField(max_length=100)
	image_url = models.TextField()
	desc = models.TextField()
	datetime = models.DateTimeField(default=now)

class Donatur(models.Model):
	program = models.ForeignKey(Program, on_delete=models.CASCADE)
	nama = models.CharField(max_length=50)
	email = models.EmailField()
	jumlah = models.BigIntegerField()
	anon = models.BooleanField(default=False)
