from django.urls import path
from program_detail import views

app_name = 'program_detail'
urlpatterns = [
    path('<int:pk>/', views.prog_detail, name='prog_detail'),
    path('<int:pk>/donate/', views.donate, name='donate'),
]
