from django.urls import path, include
from tugas1_crowdFunding.views import index, newsView

app_name = 'tugas1_crowdFunding'
urlpatterns = [
    path('', index, name='index'),
    path('news/<int:news_id>', newsView, name='news'),
]
