from django.db import models

# Create your models here.
class News(models.Model):
    judul = models.CharField(max_length=100)
    isi_berita = models.TextField(max_length=1000)
    foto = models.TextField(max_length=9999999)
