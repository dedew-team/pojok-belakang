from django.shortcuts import render
from functools import reduce
import operator
from django.db.models import Q
from tugas1_crowdFunding.models import News
from program_detail.models import Program

def index(request):
	newslist = News.objects.all()
	context = {
		'news_list': newslist,
	}
	return render(request, 'tugas1_crowdFunding/landing_page.html', context)

def newsView(request, news_id):
	news = News.objects.get(pk=news_id)
	programs = find_related(news)

	context = {
		'title': news.judul,
		'news_content': news,
		'programs':programs
	}

	return render(request, 'tugas1_crowdFunding/news_page.html', context)

def find_related(news):
	name = news.judul.strip()
	query = reduce(operator.and_, (Q(title__contains = item) for item in name))
	return Program.objects.filter(query)
