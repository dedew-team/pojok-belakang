from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from tugas1_crowdFunding.views import index, newsView
from tugas1_crowdFunding.models import News

class Hello_Web_Unit_Test(TestCase):
	def test_tugas1_crowdFunding_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_tugas1_crowdFunding_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'tugas1_crowdFunding/landing_page.html')

	def test_tugas1_crowdFunding_using_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_tugas1_crowdFunding_news(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('News', html_response)

class News_Unit_Test(TestCase):
	def test_news_exist(self):
		new_news = News.objects.create(judul='Bantu Saya Melupakan Dia', isi_berita='Saya mencintai dia, tapi apakah dia mencintai saya balik? Mari bantu saya melupakan dia karena belum tentu dia peduli :(')
		response = Client().get('/news/1')
		found = resolve('/news/1')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'tugas1_crowdFunding/news_page.html')
		self.assertEqual(found.func, newsView)
