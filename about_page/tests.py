from django.test import TestCase, Client
from django.urls import resolve
from django.utils.timezone import now
from .models import *
from .views import *
from .forms import *


# Create your tests here.
class UnitTest(TestCase):

    def test_if_models_return_dict(self):
        datetimee = now()
        new_comment = Comments.objects.create(name="Ujang", comment="Mantappp", datetime=datetimee)
        dicti = {
            "name" : "Ujang",
            "comment" : "Mantappp",
            "datetime" : datetimee,
        }
        self.assertEqual(new_comment.as_dict(), dicti)

    def test_success_if_url_is_json(self):
        response = self.client.post('/about/api/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'comments': []}
        )
    
    def test_if_using_index_function(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about_page)

    def test_if_using_index_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_if_post_schedule_succ(self):
        response_post = Client().post('/about/', 
        {'name' : "Senam", 'comment' : 'Lapangan Gor', "datetime" : now})
        self.assertEqual(response_post.status_code, 302)

    def test_form_validation_for_blank_items(self):
            form = CommentForm(data={'name' : '', 'comment' : ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['name'],
                ["This field is required."]
            )

    


