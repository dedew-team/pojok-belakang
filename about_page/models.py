from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

# Create your models here.
class Comments(models.Model):
    name = models.CharField(max_length=1000)
    comment = models.TextField(max_length=99999)
    datetime = models.DateTimeField(default=now)

    def as_dict(self):
        return {
            'name' : self.name,
            'comment' : self.comment,
            'datetime' : self.datetime,
        }