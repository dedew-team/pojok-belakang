from django.shortcuts import render, redirect, reverse
from .models import *
from .forms import *
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers

# Create your views here.

def about_page(request):
    form = CommentForm()
    # if (request.user.is_authenticated):
    if (request.method == "POST"):
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    return render(request, 'about.html', context={'form' : form})

def about_json(request):
    comments = [ obj.as_dict() for obj in Comments.objects.all().order_by('datetime') ]
    data = {"comments" : comments}
    return JsonResponse(data, safe=False)
