from .models import *
from django import forms
from django.forms import ModelForm

class CommentForm(ModelForm):
    class Meta:
        model = Comments
        fields = ["name", "comment"]

        widgets = {
            "name" : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'What name do you want to be displayed?'}),
            "comment" : forms.Textarea(attrs={'class' : 'form-control', 'placeholder' : 'Give us a nice testimony!'})
        }