from django.urls import path
from .views import *

app_name = "about_page"
urlpatterns = [
    path('', about_page, name='about_page'),
    path('api/', about_json, name='about_json'),
]
