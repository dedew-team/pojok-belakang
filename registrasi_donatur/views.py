from django.shortcuts import render, redirect
from registrasi_donatur.models import RegistrasiDonatur
from registrasi_donatur.form import RegitrasiDonaturForm
from django.contrib.auth import logout

# Create your views here.
def index(request):
    form = RegitrasiDonaturForm()

    context = {
        'title': 'Registration Form',
        'form': form
    }
    return render(request, 'registrasi_donatur/form_registrasi_donatur.html', context)

def addDonator(request):
    if request.method == 'POST':
        form = RegitrasiDonaturForm(request.POST)
        if form.is_valid():
            RegistrasiDonatur.objects.create(nama=request.POST['nama'],
                                             tanggal_lahir=request.POST['tanggal_lahir'],
                                             email=request.POST['email'],
                                             user_password=request.POST['user_password'])
            return redirect('/registrasi/success/')
        else:
            return redirect('/registrasi/error/')

def registrasiSuccess(request):
        return render(request, 'registrasi_donatur/form_registrasi_success.html', {'title': 'Registration Succes!'})

def registrasiError(request):
        return render(request, 'registrasi_donatur/form_registrasi_error.html', {'title': 'Registration Failed!'})

def logout(request):
        request.session.flush()
        return render(request, 'tugas1_crowdFunding/landing_page.html')