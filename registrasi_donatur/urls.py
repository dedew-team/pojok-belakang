from django.urls import path
from registrasi_donatur import views

app_name = 'registrasi_donatur'
urlpatterns = [
    path('', views.index, name='index'),
    path('tambah-donatur/', views.addDonator, name='addDonator'),
    path('success/', views.registrasiSuccess, name='registrasiSuccess'),
    path('error/', views.registrasiError, name='registrasiError'),
    
]
