from django.apps import AppConfig


class RegistrasiDonaturConfig(AppConfig):
    name = 'registrasi_donatur'
