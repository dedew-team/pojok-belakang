from django.db import models

# Create your models here.
class RegistrasiDonatur(models.Model):
    nama = models.CharField(max_length=50, blank=False)
    tanggal_lahir = models.DateField(blank=False)
    email = models.EmailField(max_length=50, unique=True, blank=False)
    user_password = models.CharField(max_length=30, blank=False)
