from django.forms import ModelForm, TextInput, DateInput, EmailInput, PasswordInput
from django.utils.translation import gettext_lazy as _
from registrasi_donatur.models import RegistrasiDonatur

class RegitrasiDonaturForm(ModelForm):
    class Meta:
        model = RegistrasiDonatur
        fields = ('nama', 'tanggal_lahir', 'email', 'user_password')
        widgets = {
            'nama': TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama'}),
            'tanggal_lahir': DateInput(attrs={'class': 'form-control', 'placeholder': 'Tanggal Lahir'}),
            'email': EmailInput(attrs={'class': 'form-control', 'placeholder': 'E-mail'}),
            'user_password': PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'})
        }
        labels = {
            'nama': _(''),
            'tanggal_lahir':  _(''),
            'email':  _(''),
            'user_password':  _(''),
        }
        help_texts = {
            'tanggal_lahir': _('<small>format: tttt-bb-hh</small>'),
            'email': _('<small>example: name@example.com</small>')
        }