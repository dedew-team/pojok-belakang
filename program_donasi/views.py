from django.shortcuts import render
from program_detail.models import *

# Create your views here.
def programs(request):
    programs_list = Program.objects.all().order_by('-datetime')
    return render(request, 'programs.html', {'title': 'Programs', 'programs' : programs_list})
