from django.apps import AppConfig


class ProgramDonasiConfig(AppConfig):
    name = 'program_donasi'
