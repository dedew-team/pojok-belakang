from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils.timezone import now
from .views import *

# Create your tests here.
class ProgramDonasiTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/programs/')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = Client().get('/programs/')
        self.assertTemplateUsed(response, 'programs.html')

    def test_using_func(self):
        found = resolve('/programs/')
        self.assertEqual(found.func, programs)

    def test_if_programs_text_exist(self):
        request = HttpRequest()
        response = programs(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Programs', html_response)

    def test_if_pojok_belakang_name_is_exist(self):
        pojokbelakang = 'Pojok Belakang'
        response= Client().get('/programs/')
        html_response = response.content.decode('utf8')
        self.assertIn(pojokbelakang, html_response)
